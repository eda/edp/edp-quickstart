<h1><center>EDP QUICK START</center></h1>

User-wise setup and installation scripts for EDP.

## DEPENDENCIES

Bring this on site

1. Compiled edp-core
1. Compiled and source edp-itk
1. Compiled and source edp-pdk-cds
1. Compiled and source edp-pdk-xt018
1. Source edp-bin
1. Source edp-py
1. Source edp-mat

## SYSTEM

1. Install [mvn](https://maven.apache.org/download.cgi)
1. Make sure `java-1.8` is installed and available
1. Create a global installation directory: `/some/path/edp`
1. Clone/Copy utilities into this directory:
    - `$ git clone https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-bin.git bin`
    - `$ git clone https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-py.git py`
    - `$ git clone https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-mat.git mat`
1. Unpack edp-core in this directory:
    - `$ tar xzvf edp-core-XYZ.tar.gz`
1. Create technology file directory `$ mkdir tech-files` and clone ITK into it
    - `$ git clone https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-itk.git`
1. Either copy or clone PDKs into `tech-files`
    - `$ git clone https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-pdk-xt018.git`
    - `$ git clone https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-pdk-cds.git`
1. Build each of them with maven in the respective directory
    - `$ mvn install`
1. Create symlink from `./edp-pdk-cds.jar -> ./target/edp-pdk-cds-*.jar` if it does not already exist
1. Create symlink from `./edp-pdk-xt018.jar -> ./target/edp-pdk-xt018-*.jar` if it does not already exist

## USER

1. Create `eda_tools` project in **XT018** PDK
    - Loading `spectre` module is important!
1. Launch Virtuoso and create libraries (Attach to `XT018_TECH`)
    - `edp_dsgn_$USER`, `edp_tb_$USER`, `evalLib_$USER`
1. Close Virtuoso
1. Change into project directory: `$ cd <eda_tools project dir>/users/$USER`
1. Run `$ sh <(curl -s "https://gitlab-forschung.reutlingen-university.de/api/v4/projects/1022/repository/files/install.sh/raw?ref=master")`
1. Launch Virtuoso
    - EDP menu should be in CIW
1. Click EDP > Start Socket
1. In `eda_tools` terminal go into `../home`
1. Do `$ source edp/bin/activate`
1. Start EDP development
