#!/bin/sh

SUCC="\033[0;32m✓\033[0m"
FAIL="\033[0;31m✗\033[0m"
WARN="\033[0;31m!\033[0m"
USER="$(whoami)"

GIT_BASE="https://gitlab-forschung.reutlingen-university.de/api/v4/projects/1022/repository/files/"
EDA_PROJ="$(pwd)"

# if ! type "conda" > /dev/null; then
#     printf "[$FAIL] conda not found\n"
#     exit 1
# else
#     CONDA_ROOT="$(conda info --envs | grep base | tr -s ' ' | cut -d' ' -f2)"
#     printf "[$SUCC] conda found at $CONDA_ROOT\n"
# fi

if ! type "mvn" > /dev/null; then
    printf "[$FAIL] mvn not found\n"
    exit 2
else
    JAVA_DEFAULT="$(mvn --version | grep runtime | cut -d' ' -f9 | xargs dirname)"
    printf "[$SUCC] mvn found with java at $JAVA_DEFAULT\n"
fi

read -p 'Absolute path to EDP Installation ($ED_EDP_HOME): ' ED_EDP_HOME
if [ -d "$ED_EDP_HOME" ]; then
    printf "[$SUCC] valid ED_EDP_HOME provided\n"
else
    printf "[$FAIL] cannot access $ED_EDP_HOME\n"
    exit 1
fi

read -p "JAVA 1.8 HOME [$JAVA_DEFAULT]: " JAVA_USR

export JAVA_HOME="${JAVA_USR:-$JAVA_DEFAULT}"
export ED_EDP_HOME="$ED_EDP_HOME"

# curl -s "$GIT_BASE/setenv/raw?ref=master" \
#     | sed -e "s|%JAVHOME%|$JAVA_HOME|" \
#     | sed -e "s|%EDPHOME%|$ED_EDP_HOME|" \
#     > $EDA_PROJ/home/setenv

echo "export JAVA_HOME=$JAVA_HOME" >> $EDA_PROJ/home/.bashrc_user
echo "export ED_EDP_JRE=$JAVA_HOME/jre/bin/java" >> $EDA_PROJ/home/.bashrc_user
echo "export ED_EDP_HOME=$ED_EDP_HOME" >> $EDA_PROJ/home/.bashrc_user
echo "export ED_SPECTRE_DISABLE_CHECK=1" >> $EDA_PROJ/home/.bashrc_user
echo "export PATH=\$ED_EDP_HOME/bin:\$PATH" >> $EDA_PROJ/home/.bashrc_user

TECH_CDS_DEFAULT="$ED_EDP_HOME/tech-files/edp-pdk-cds/edp-pdk-cds.xml"
read -p "Absolute path to CDS Technology file [$TECH_CDS_DEFAULT]: " TECH_CDS_USR
TECH_CDS="${TECH_CDS_USR:-$TECH_CDS_DEFAULT}"
if [ -z "$TECH_CDS" ]; then
    printf "[$FAIL] No path to edp-pdk-cds.xml provided\n"
    exit 3
elif  [ -e "$TECH_CDS" ]; then
    printf "[$SUCC] found $(basename $TECH_CDS)\n"
else 
    printf "[$FAIL] cannot access $TECH_CDS\n"
    exit 4
fi

TECH_XT018_DEFAULT="$ED_EDP_HOME/tech-files/edp-pdk-xt018/edp-pdk-xt018.xml"
read -p "Absolute path to XT018 Technology file [$TECH_XT018_DEFAULT]: " TECH_XT018_USR
TECH_XT018="${TECH_XT018_USR:-$TECH_XT018_DEFAULT}"
if [ -z "$TECH_XT018" ]; then
    printf "[$FAIL] No path to edp-pdk-cds.xml provided\n"
    exit 5
elif  [ -e "$TECH_XT018" ]; then
    printf "[$SUCC] found $(basename $TECH_XT018)\n"
else 
    printf "[$FAIL] cannot access $TECH_XT018\n"
    exit 6
fi

read -p 'Simulation Directory: ' SIM_DIR
if [ -z "$TECH_CDS" ]; then
    printf "[$WARN] No simulation data directory provided, using $EDA_PROJ/home\n"
elif [ -d $SIM_DIR ]; then
    printf "[$SUCC] using $SIM_DIR\n"
else
    printf "[$FAIL] cannot access $SIM_DIR\n"
    exit 7
fi

curl -s "$GIT_BASE/.edpinit.json/raw?ref=master" \
    | sed -e "s|%WORKDIR%|$EDA_PROJ/cds|" \
    | sed -e "s|%LOGDIR%|$EDA_PROJ/home|" \
    | sed -e "s|%TECHCDS%|$TECH_CDS|" \
    | sed -e "s|%TECHXKIT%|$TECH_XT018|" \
    | sed -e "s|%SIMDIR%|$SIM_DIR|" \
    | sed -e "s|%USR%|$USER|g" \
    > $EDA_PROJ/home/.edpinit.json

if [ -e $EDA_PROJ/home/.edpinit.json ]; then
    printf "[$SUCC] .edpinit.json setup\n"
else
    printf "[$FAIL] failed to set up .edpinit.json\n"
    exit 8
fi

CDSINIT="$EDA_PROJ/cds/.cdsinit_user"
if [ -e $CDSINIT ]; then
    echo '(load (strcat (getShellEnvVar "ED_EDP_HOME") "/core/init/init.ile"))' >> $CDSINIT
    echo '(EDedpInitMenu)' >> $CDSINIT
    printf "[$SUCC] patched $CDSINIT\n"
else
    printf "[$FAIL] cannot access $CDSINIT\n"
    exit 9
fi

# curl -s "$GIT_BASE/environment.yml/raw?ref=master" > "$EDA_PROJ/environment.yml"
# conda env create -f "$EDA_PROJ/environment.yml"
# source $CONDA_ROOT/bin/activate edp
# rm -v "$EDA_PROJ/environment.yml"
# echo "auto_activate_base: false" >> "$EDA_PROJ/home/.condarc"
# echo ". $CONDA_ROOT/etc/profile.d/conda.sh" >> "$EDA_PROJ/home/.bashrc_user"

# pushd "$ED_EDP_HOME/py"
# pip install . 
# popd

# source $EDA_PROJ/home/setenv
cd $EDA_PROJ/home
curl -s 'https://gitlab-forschung.reutlingen-university.de/api/v4/projects/1018/repository/files/cmos_inv_sch_gen.ipynb/raw?ref=main' > cmos_inv_sch_gen.ipynb
python -m venv $EDA_PROJ/home/edp
printf "\n[$SUCC] Python environment set up\n"
source $EDA_PROJ/home/edp/bin/activate
pip install numpy scipy pandas matplotlib plotly seaborn tqdm pyyaml jupyterlab jpype1
pip install git+https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-py.git
printf "\n[$SUCC] Python dependencies installed\n"
deactivate

#printf "\n[$SUCC] Befor launching virtuoso source $EDA_PROJ/home/setenv in your shell\n"

exit 0
